const express = require('express');
const hbs = require('express-handlebars');
require('dotenv').config();
const path = require('path')
const session = require('express-session')
require('./lib/database');
const methodOverride = require('method-override')
const helmet = require('helmet');
const cors = require('cors');
const winston = require("winston");

const app = express();

app.use('*', (req, res, next) => {
    res.locals.absoluteUrl = process.env.URLROOT
    next();
})

//override html methods
app.use(methodOverride('_method'))

//set handlebars as the main view engine
app.engine('handlebars', hbs({
    helpers: {
        rootpath: process.env.ROOTPATH,
        escape: variable => variable.replace(/(['"])/g, '\\$1')
    }
}));
app.set('view engine', 'handlebars');

//parsing data
//parse body
app.use(express.json()) // for parsing application /json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


// console.log(path.join(__dirname, "assets"));

//set the path to the assets directory
app.use(express.static(path.join(__dirname, "assets")));

//set up passport authentication and enable use of sessions 
app.use(session({ secret: process.env.SECRET, resave: false, saveUninitialized: true }))

app.use('/', require('./lib/routes/routes'));
app.use('/auth', require('./lib/routes/auth'));
app.use('/admin', require('./lib/routes/admin'))
// console.log(app);

//check node environment 
const PORT = (process.env.NODE_ENV == 'production') ? process.env.PORT : process.env.SERVER_PORT;

app.use(helmet());
app.use(cors());

//setup logger
//setup a logger 
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' }),
    ],
});

// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}


app.listen(PORT, () => console.log(`Express app listening on port ${PORT}!`))